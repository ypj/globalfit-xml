#!/usr/bin/env python3

from lattice_info.base import *

compath = "/home/djm/projects/lattice_data/32I/b2p37/ms0p0186_ml0p0047/unitary_ppscatt/analysis/gf"

info = {'id': '32Ifine',
        'path': compath,
        'Latt': [32, 32, 32, 64],
        'Action': 'DWFplusI',
        'ChisqWeight': 1,
        'RWargs': {
            'mq': [0.0186, 0, 0.0186, 0.0186], # [start, inc, end, data]
        },
        'mres_type': 'MresChiralLimit',
        'mres_chiral': {
            'BootType': 'BootFile',
            'ParamFile': compath + "/mres/mass.boot",
            'ParamNumber': 0,
        },
        'Ensembles': {
            'Ml_sea': [0.0047],          
            'EnsParams': {
                'PerBin': 1,
                'Boots': 500,
                'Resampling': 'Jackknife',
                'FOpenerArg': {
                    'ensemble_tag': ['32I_fine_ml0.0047'],
                    'traj_start': [0],
                    'traj_increment': 1,
                    'traj_lessthan': [12],    
                },      
            },          
            'ValParams': {
                'mh': [0.0186],
                'ml': [0.0047],
                'mcombo': [0.0047],
                'mhcuts': [0.0186],                    
                'mlcuts': [0.0047],
                'exclude': [[0.0,0.0]], # deprecated?
                'Mpi': {
                    'ParamFile': compath + "/mps_@MX_@MY/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fpi': {
                    'ParamFile': compath + "/fps_@MX_@MY/fps_ZVnorm.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'MK': {
                    'ParamFile': compath + "/mps_@MY_@MX/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fK': {
                    'ParamFile': compath + "/fps_@MY_@MX/fps_ZVnorm.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'MOmega': {
                    'franges': [[0,0]],
                    'ParamFile': compath + "/momega/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0', 
                    'Constant': '0',                     
                }, 
                'sqrt_t0': {
                    'ParamFile': compath + "/wflow/boot/sqrt_t0", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'w0': {
                    'ParamFile': compath + "/wflow/boot/w0", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'a02': {
                    'ParamFile': compath + "/a0/mass.boot",
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
            },          
        },
        'plot_my': [1e-5, 0.02], # [start, end]
       }


class LatticeInfo(LatticeInfoBase):

    def __init__(self, info):

        self.info = info
        LatticeInfoBase.__init__(self, info)
    

XMLWriter = LatticeInfo(info)
