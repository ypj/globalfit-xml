#!/usr/bin/env python3

from lattice_info.base import *

compath = "/home/djm/projects/lattice_data/24ID/b1p633/ms0p0850_ml0p00107/unitary/analysis/gf"

info = {'id': '24ID',
        'path': compath,
        'Latt': [24, 24, 24, 64],
        'Action': 'DWFplusID',
        'ChisqWeight': 1,
        'RWargs': {
            'mq': [0.085, 0, 0.085, 0.085], # [start, inc, end, data]
        },
        'mres_type': 'MresChiralLimit',
        'mres_chiral': {
            'BootType': '', # none?
            'ParamFile': compath + "/mres/mass.boot",
            'ParamNumber': 0,
        },
        'Ensembles': {
            'Ml_sea': [0.00107],          
            'EnsParams': {
                'PerBin': 1,
                'Boots': 500,
                'Resampling': 'Jackknife',
                'FOpenerArg': {
                    'ensemble_tag': ['24ID'],
                    'traj_start': [0],
                    'traj_increment': 1,
                    'traj_lessthan': [133],    
                },      
            },          
            'ValParams': {
                'mh': [0.085],
                'ml': [0.00107],
                'mcombo': [0.00107],
                'mhcuts': [0.085],                    
                'mlcuts': [0.00107],
                'exclude': [[0.0,0.0]], # deprecated?
                'Mpi': {
                    'ParamFile': compath + "/mps_@MX_@MY/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fpi': {
                    'ParamFile': compath + "/fps_@MX_@MY/fps_ZVnorm.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'MK': {
                    'ParamFile': compath + "/mps_@MY_@MX/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fK': {
                    'ParamFile': compath + "/fps_@MY_@MX/fps_ZVnorm.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'MOmega': {
                    'franges': [[0,0]],
                    'ParamFile': compath + "/momega/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0', 
                    'Constant': '0',                     
                }, 
                'sqrt_t0': {
                    'ParamFile': compath + "/wflow/boot/sqrt_t0", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'w0': {
                    'ParamFile': compath + "/wflow/boot/w0", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'a02': { 
                    'ParamFile': compath + "/a0/mass.boot",
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
            },          
        },
        'plot_my': [1e-5, 0.06], # [start, end]
       }


class LatticeInfo(LatticeInfoBase):

    def __init__(self, info):

        self.info = info
        LatticeInfoBase.__init__(self, info)
    
    
    def appendSectionWilsonFlow(self, i, obs, ValenceCombos, mydata):

        info = self.info

        Ml_sea = info['Ensembles']['Ml_sea']
        ValParams = info['Ensembles']['ValParams']
            
        mlcut = ValParams['mlcuts'][i]
        mhcut = ValParams['mhcuts'][i]
        
        # exclude sqrt_t0
        if obs == 'sqrt_t0':
            mhcut = 0

        params = ValParams[obs]
        for Mx in [info['RWargs']['mq'][3]]:
            vc_Mx = copy.deepcopy(ValenceCombos)
            vc_Mx.find('./elem/Mx').text = str(Mx)
            vc_My = copy.deepcopy(mydata)
            vc_My.find('./elem/My').text = str(Mx)
            ParamFile = params['ParamFile'].replace('@MLSEA', str(Ml_sea[i]))
            vc_My.find('./elem/boot/ParamFile').text = ParamFile
            for opt in ['BootType', 'ParamNumber', 'Constant']:
                vc_My.find('./elem/boot/'+opt).text = params[opt]
            if Mx > mhcut:
                vc_My.find('./elem/include_in_fit').text = 'false'
            else:
                vc_My.find('./elem/include_in_fit').text = 'true'
            
            vc_Mx = joinTrees(vc_Mx, './elem/mydata', vc_My, './elem')
            self.ens = joinTrees(self.ens, './elem/'+obs+'_data', vc_Mx, './elem')


XMLWriter = LatticeInfo(info)
