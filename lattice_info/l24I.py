#!/usr/bin/env python3

from lattice_info.base import *

compath = "/home/ckelly/projects/24nt64_fullanalysis"

info = {'id': '24I',
        'path': compath,
        'Latt': [24, 24, 24, 64],
        'Action': 'DWFplusI',
        'ChisqWeight': 1,
        'RWargs': {
            'mq': [0.04, -0.00025, 0.033, 0.04], # [start, inc, end, data]
        },
        'mres_type': 'MresChiralLimit',
        'mres_chiral': {
            'BootType': 'BootFileMultiReweighted',
            'ParamFile': compath + "/chiral/mres/jackknife/reweighted/boot/"
                         + "mres_extrapcoords.boot",
            'ParamNumber': 1,
        },
        'Ensembles': {
            'Ml_sea': [0.005, 0.01],          
            'EnsParams': {
                'PerBin': 2,
                'Boots': 500,
                'Resampling': 'Jackknife',
                'FOpenerArg': {
                    'ensemble_tag': [0.005, 0.01],
                    'traj_start': [900, 1460],
                    'traj_increment': 40,
                    'traj_lessthan': [8980, 8580],    
                },      
            },          
            'ValParams': {
                'mh': [0.04, 0.03],
                'ml': [0.02, 0.01, 0.005, 0.001],
                'mcombo': [0.04, 0.03, 0.02, 0.01, 0.005, 0.001],
                'mhcuts': [0.04, 0],                    
                'mlcuts': [0.005, 0],
                'exclude': [[0.0,0.0], [0.0,0.0]], # deprecated?
                'Mpi': {
                    'ParamFile': compath + "/@MLSEA/fpi/jackknife/reweighted/boot/"
                                 + "fit_@MX_@MY_10_50.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fpi': {
                    'ParamFile': compath + "/@MLSEA/fpi/jackknife/reweighted/fpi_ZV_new_080414/boot/"
                                 + "fpi_@MX_@MY_10_50.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'MK': {
                    'ParamFile': compath + "/@MLSEA/fpi/jackknife/reweighted/boot/"
                                 + "fit_@MX_@MY_10_50.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fK': {
                    'ParamFile': compath + "/@MLSEA/fpi/jackknife/reweighted/fpi_ZV_new_080414/boot/"
                                 + "fpi_@MX_@MY_10_50.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'MOmega': {
                    'franges': [[5,11], [5,11], [5,11]],
                    'ParamFile': compath + "/@MLSEA/momega/jackknife/reweighted/boot/"
                                 + "fit_@MX_@FRMIN_@FRMAX.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '1',
                    'Constant': '0',                     
                }, 
                'sqrt_t0': {
                    'ParamFile': compath + "/@MLSEA/wflow/boot/sqrt_t0", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'w0': {
                    'ParamFile': compath + "/@MLSEA/wflow/boot/w0", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'a02': {
                    'ParamFile': "/home/djm/projects/lattice_data/24I/b2p13/"
                                 + "ms0p04_ml@MLSEA0p/unitary/analysis/gf/a0/boot/a0",
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
            },          
        },
        'plot_my': [1e-5, 0.045], # [start, end]
       }


class LatticeInfo(LatticeInfoBase):

    def __init__(self, info):

        self.info = info
        LatticeInfoBase.__init__(self, info)
    

XMLWriter = LatticeInfo(info)
