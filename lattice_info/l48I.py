#!/usr/bin/env python3

from lattice_info.base import *

compath = "/home/djm/projects/lattice_data/48I/fine/analysis/gf"

info = {'id': '48I',
        'path': compath,
        'Latt': [48, 48, 48, 96],
        'Action': 'DWFplusI',
        'ChisqWeight': 1,
        'RWargs': {
            'mq': [0.02144, 0, 0.02144, 0.02144], # [start, inc, end, data]
        },
        'mres_type': 'MresChiralLimit',
        'mres_chiral': {
            'BootType': 'BootFile', 
            'ParamFile': compath + "/mres/mass.boot",
            'ParamNumber': 0,
        },
        'Ensembles': {
            'Ml_sea': [0.002144],          
            'EnsParams': {
                'PerBin': 1,
                'Boots': 500,
                'Resampling': 'Jackknife',
                'FOpenerArg': {
                    'ensemble_tag': ['48I_fine_ml0.002144'],
                    'traj_start': [0],
                    'traj_increment': 1,
                    'traj_lessthan': [36],    
                },      
            },          
            'ValParams': {
                'mh': [0.02144],
                'ml': [0.002144],
                'mcombo': [0.002144],
                'mhcuts': [0.02144],                    
                'mlcuts': [0.002144],
                'exclude': [[0.0,0.0]], # deprecated?
                'Mpi': {
                    'ParamFile': compath + "/mps_@MX_@MY/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fpi': {
                    'ParamFile': compath + "/fps_@MX_@MY/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'MK': {
                    'ParamFile': compath + "/mps_@MY_@MX/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fK': {
                    'ParamFile': compath + "/fps_@MY_@MX/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'sqrt_t0': {
                    'ParamFile': compath + "/wflow/boot/sqrt_t0", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'w0': {
                    'ParamFile': compath + "/wflow/boot/w0", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
            },          
        },
        'plot_my': [1e-5, 0.04], # [start, end]
       }


class LatticeInfo(LatticeInfoBase):

    def __init__(self, info):

        self.info = info
        LatticeInfoBase.__init__(self, info)
    

XMLWriter = LatticeInfo(info)
