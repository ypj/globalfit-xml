#!/usr/bin/env python3

from lattice_info.base import *

compath = "/home/ckelly/projects/32nt64_DSDR_fullanalysis"

info = {'id': '32ID',
        'path': compath,
        'Latt': [32, 32, 32, 64],
        'Action': 'DWFplusID',
        'ChisqWeight': 1,
        'RWargs': {
            'mq': [0.045, 0.00025, 0.047, 0.045], # [start, inc, end, data]
        },
        'mres_type': 'MresChiralLimit',
        'mres_chiral': {
            'BootType': 'BootFileMultiReweighted',
            'ParamFile': compath + "/chiral/mres/jackknife/reweighted/boot/"
                         + "mres_extrapcoords.boot",
            'ParamNumber': 1,
        },
        'Ensembles': {
            'Ml_sea': [0.001, 0.0042],          
            'EnsParams': {
                'PerBin': 4,
                'Boots': 500,
                'Resampling': 'Jackknife',
                'FOpenerArg': {
                    'ensemble_tag': ['32ID_0.001', '32ID_0.0042'],
                    'traj_start': [500, 600],
                    'traj_increment': 8,
                    'traj_lessthan': [1940, 1784],    
                },      
            },          
            'ValParams': {
                'mh': [0.055, 0.045, 0.035],
                'ml': [0.008, 0.0042, 0.001, 0.0001],
                'mcombo': [0.055, 0.045, 0.035, 0.008, 0.0042, 0.001, 0.0001],
                'mhcuts': [0.055, 0.055],                    
                'mlcuts': [0.008, 0.008],
                'exclude': [[0.0,0.0], [0.0,0.0]], # deprecated?
                'Mpi': {
                    'ParamFile': compath + "/m@MLSEA/fpi/jackknife/reweighted/boot/"
                                 + "fit_@MX_@MY_8_63.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fpi': {
                    'ParamFile': compath + "/m@MLSEA/fpi/jackknife/reweighted/boot/"
                                 + "fpi_@MX_@MY_8_63_ZVnorm.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'MK': {
                    'ParamFile': compath + "/m@MLSEA/fpi/jackknife/reweighted/boot/"
                                 + "fit_@MX_@MY_8_63.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fK': {
                    'ParamFile': compath + "/m@MLSEA/fpi/jackknife/reweighted/boot/"
                                 + "fpi_@MX_@MY_8_63_ZVnorm.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'MOmega': {
                    'franges': [[3,10], [3,10]],
                    'ParamFile': compath + "/m@MLSEA/momega/jackknife/reweighted/boot/"
                                 + "fit_@MX_@FRMIN_@FRMAX.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '1', 
                    'Constant': '0',                     
                }, 
                'sqrt_t0': {
                    'ParamFile': compath + "/m@MLSEA/wflow/boot/sqrt_t0", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'w0': {
                    'ParamFile': compath + "/m@MLSEA/wflow/boot/w0", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'a02': { 
                    # file path is different for each Ml_sea
                    'ParamFile': ["/home/djm/projects/lattice_data/32ID/b1p75/"
                                  + "ms0p046_ml@MLSEA0p/pq/gf/a0/boot/a0",
                                  "/home/djm/projects/lattice_data/32ID/b1p75/"
                                  + "ms0p046_ml@MLSEA0p/unitary/analysis/gf/a0/boot/a0"],
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
            },          
        },
        'plot_my': [1e-5, 0.06], # [start, end]
       }


class LatticeInfo(LatticeInfoBase):

    def __init__(self, info):

        self.info = info
        LatticeInfoBase.__init__(self, info)
        
    
    def appendSectionPionScattering(self, i, obs, ValenceCombos, mydata):

        info = self.info

        Ml_sea = info['Ensembles']['Ml_sea']
        ValParams = info['Ensembles']['ValParams']
            
        mlcut = ValParams['mlcuts'][i]
        mhcut = ValParams['mhcuts'][i]
        Mx = Ml_sea[i]
        
        # overwrite for the first input (Mx, Ml_sea, incl) = (0.0001, 0.001, False)
        if Ml_sea[i] == 0.001:
            mlcut = 0
            Mx = 0.0001
        
        params = ValParams[obs]
        
        vc_Mx = copy.deepcopy(ValenceCombos)
        vc_Mx.find('./elem/Mx').text = str(Mx)
        vc_My = copy.deepcopy(mydata)
        vc_My.find('./elem/My').text = str(Mx)
        #ParamFile = params['ParamFile'].replace('@MLSEA0p', str(Mx).replace('.','p'))
        ParamFile = params['ParamFile'][i].replace('@MLSEA0p', str(Ml_sea[i]).replace('.','p'))
        vc_My.find('./elem/boot/ParamFile').text = ParamFile
        for opt in ['BootType', 'ParamNumber', 'Constant']:
            vc_My.find('./elem/boot/'+opt).text = params[opt]
        if Mx > mlcut:
            vc_My.find('./elem/include_in_fit').text = 'false'
        else:
            vc_My.find('./elem/include_in_fit').text = 'true'
        
        vc_Mx = joinTrees(vc_Mx, './elem/mydata', vc_My, './elem')
        self.ens = joinTrees(self.ens, './elem/'+obs+'_data', vc_Mx, './elem')


XMLWriter = LatticeInfo(info)
