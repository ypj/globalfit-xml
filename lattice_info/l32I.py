#!/usr/bin/env python3

from lattice_info.base import *

compath = "/home/ckelly/projects/32nt64_fullanalysis"

info = {'id': '32I',
        'path': compath,
        'Latt': [32, 32, 32, 64],
        'Action': 'DWFplusI',
        'ChisqWeight': 1,
        'RWargs': {
            'mq': [0.03, -0.0005, 0.026, 0.03], # [start, inc, end, data]
        },
        'mres_type': 'MresChiralLimit',
        'mres_chiral': {
            'BootType': 'BootFileMultiReweighted',
            'ParamFile': compath + "/chiral/mres/jackknife/reweighted/boot/"
                         + "mres_extrapcoords.boot",
            'ParamNumber': 1,
        },
        'Ensembles': {
            'Ml_sea': [0.004, 0.006, 0.008],          
            'EnsParams': {
                'PerBin': 4,
                'Boots': 500,
                'Resampling': 'Jackknife',
                'FOpenerArg': {
                    'ensemble_tag': [0.004, 0.006, 0.008],
                    'traj_start': [260, 500, 260],
                    'traj_increment': 10,
                    'traj_lessthan': [3260, 3620, 2780],    
                },      
            },          
            'ValParams': {
                'mh': [0.03, 0.025],
                'ml': [0.008, 0.006, 0.004, 0.002],
                'mcombo': [0.03, 0.025, 0.008, 0.006, 0.004, 0.002],
                'mhcuts': [0.03, 0.03, 0],                    
                'mlcuts': [0.006, 0.006, 0],
                'exclude': [[0.03,0.03], [0.03,0.025], [0.025,0.025]], # deprecated?
                'Mpi': {
                    'ParamFile': compath + "/m@MLSEA/fpi/jackknife/reweighted/boot/"
                                 + "fit_@MX_@MY_12_52.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fpi': {
                    'ParamFile': compath + "/m@MLSEA/fpi/jackknife/reweighted/fpi_ZV_new_080414/boot/"
                                 + "fpi_@MX_@MY_12_52.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'MK': {
                    'ParamFile': compath + "/m@MLSEA/fpi/jackknife/reweighted/boot/"
                                 + "fit_@MX_@MY_12_52.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fK': {
                    'ParamFile': compath + "/m@MLSEA/fpi/jackknife/reweighted/fpi_ZV_new_080414/boot/"
                                 + "fpi_@MX_@MY_12_52.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'MOmega': {
                    'franges': [[7,13], [7,13], [7,13]],
                    'ParamFile': compath + "/m@MLSEA/momega/jackknife/reweighted/boot/"
                                 + "fit_@MX_@FRMIN_@FRMAX.boot", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '1',
                    'Constant': '0',                     
                }, 
                'sqrt_t0': {
                    'ParamFile': compath + "/m@MLSEA/wflow/boot/sqrt_t0", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'w0': {
                    'ParamFile': compath + "/m@MLSEA/wflow/boot/w0", 
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'a02': {
                    'ParamFile': "/home/djm/projects/lattice_data/32I/b2p25/"
                                 + "ms0p03_ml@MLSEA0p/unitary/gf/a0/boot/a0",
                    'BootType': 'BootFileMultiReweighted',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
            },          
        },
        'plot_my': [1e-5, 0.032], # [start, end]
       }


class LatticeInfo(LatticeInfoBase):

    def __init__(self, info):

        self.info = info
        LatticeInfoBase.__init__(self, info)
    

XMLWriter = LatticeInfo(info)
