#!/usr/bin/env python3

from lattice_info.base import *

compath = "/home/djm/projects/lattice_data/32ID/b1p633/ms0p0850_ml0p00107/unitary/analysis/gf"

info = {'id': '32ID_M3',
        'path': compath,
        'Latt': [32, 32, 32, 64],
        'Action': 'DWFplusID',
        'ChisqWeight': 1,
        'RWargs': {
            'mq': [0.0850, 0, 0.0850, 0.0850], # [start, inc, end, data]
        },
        'mres_type': 'MresChiralLimit',
        'mres_chiral': {
            'BootType': '', # none?
            'ParamFile': compath
                         + "/mres/mass.boot",
            'ParamNumber': 0,
        },
        'Ensembles': {
            'Ml_sea': [0.00107],          
            'EnsParams': {
                'PerBin': 1,
                'Boots': 500,
                'Resampling': 'Jackknife',
                'FOpenerArg': {
                    'ensemble_tag': ['32ID_b1.633_ms0.085'],
                    'traj_start': [0],
                    'traj_increment': 1,
                    'traj_lessthan': [50],    
                },      
            },          
            'ValParams': {
                'mh': [0.085],
                'ml': [0.00107],
                'mcombo': [0.00107],
                'mhcuts': [0.085],                    
                'mlcuts': [0.00107],
                'exclude': [[0.0,0.0]], # deprecated?
                'Mpi': {
                    'ParamFile': compath + "/mps_@MX_@MY/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fpi': {
                    'ParamFile': compath + "/fps_@MX_@MY/fps_ZVnorm.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'MK': {
                    'ParamFile': compath + "/mps_@MY_@MX/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fK': {
                    'ParamFile': compath + "/fps_@MY_@MX/fps_ZVnorm.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'MOmega': {
                    'franges': [[0,0]],
                    'ParamFile': compath + "/momega/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0', 
                    'Constant': '0',                     
                }, 
                'a02': { 
                    'ParamFile': compath + "/a0/mass.boot",
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
            },          
        },
        'plot_my': [1e-5, 0.06], # [start, end]
       }


class LatticeInfo(LatticeInfoBase):

    def __init__(self, info):

        self.info = info
        LatticeInfoBase.__init__(self, info)
    

XMLWriter = LatticeInfo(info)
