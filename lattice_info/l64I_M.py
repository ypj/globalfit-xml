#!/usr/bin/env python3

from lattice_info.base import *

compath = "/home/djm/projects/lattice_data/64I/b2p25/ms0p02661_ml0p000678/unitary/analysis_ppscatt/gf"

info = {'id': '64I_M',
        'path': compath,
        'Latt': [64, 64, 64, 128],
        'Action': 'DWFplusI',
        'ChisqWeight': 1,
        'RWargs': {
            'mq': [0.02661, 0, 0.02661, 0.02661], # [start, inc, end, data]
        },
        'mres_type': 'MresChiralLimit',
        'mres_chiral': {
            'BootType': 'BootFile',
            'ParamFile': compath + "/mres/mass.boot",
            'ParamNumber': 0,
        },
        'Ensembles': {
            'Ml_sea': [0.000678],          
            'EnsParams': {
                'PerBin': 1,
                'Boots': 500,
                'Resampling': 'Jackknife',
                'FOpenerArg': {
                    'ensemble_tag': ['64I_ml0.000678'],
                    'traj_start': [0],
                    'traj_increment': 1,
                    'traj_lessthan': [13],    
                },      
            },          
            'ValParams': {
                'mh': [0.02661],
                'ml': [0.000678],
                'mcombo': [0.000678],
                'mhcuts': [0.02661],                    
                'mlcuts': [0.000678],
                'exclude': [[0.0,0.0]], # deprecated?
                'Mpi': {
                    'ParamFile': compath + "/mps_@MX_@MY/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fpi': {
                    'ParamFile': compath + "/fps_@MX_@MY/fps_ZVnorm.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'MK': {
                    'ParamFile': compath + "/mps_@MY_@MX/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                },
                'fK': {
                    'ParamFile': compath + "/fps_@MY_@MX/fps_ZVnorm.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'MOmega': {
                    'franges': [[0,0]],
                    'ParamFile': compath + "/momega/mass.boot", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0', 
                    'Constant': '0',                     
                }, 
                'sqrt_t0': {
                    'ParamFile': compath + "/wflow/boot/sqrt_t0", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'w0': {
                    'ParamFile': compath + "/wflow/boot/w0", 
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
                'a02': {
                    'ParamFile': compath + "/a0/mass.boot",
                    'BootType': 'BootFile',        
                    'ParamNumber': '0',
                    'Constant': '0',                     
                }, 
            },          
        },
        'plot_my': [1e-5, 0.04], # [start, end]
       }


class LatticeInfo(LatticeInfoBase):

    def __init__(self, info):

        self.info = info
        LatticeInfoBase.__init__(self, info)
    

XMLWriter = LatticeInfo(info)
