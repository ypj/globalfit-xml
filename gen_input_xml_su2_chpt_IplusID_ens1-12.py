#!/usr/bin/env python3

import xml.etree.ElementTree as ET
from xml.dom import minidom
import os, sys
import copy
#from lattice_info.base import LatticeInfoBase
from lattice_info import l32I, l24I, l48I_M, l64I_M, l32Ifine, l32ID, l24ID, l32ID_M1, l32ID_M2, l32ID_M3, l48I


def LoadXML(file, show=False):
    
    tree = ET.parse(file)
    
    if show:
        print(ET.tostring(tree.getroot(), encoding='utf8', short_empty_elements=False).decode('utf8'))
        print('\n')
    
    return tree


def SaveXML(tree, save_file):
    
    #tree.write(save_file, 
    #           short_empty_elements=False, 
    #           encoding='utf8', xml_declaration=True)

    # for a pretty indentation
    for elem in tree.getroot().iter():
        if elem.text:
            elem.text = elem.text.strip()
        if elem.tail:
            elem.tail = elem.tail.strip()
    
    tree_str = ET.tostring( tree.getroot(), encoding='utf8', short_empty_elements=False )
    tree_str = minidom.parseString(tree_str).toprettyxml(indent="  ")

    with open(save_file,'w') as fout:
        fout.write(tree_str)


def joinTrees(parent, pnode, child, cnode, text=''):

    combined = copy.deepcopy(parent)
    joint = combined.getroot().find(pnode)
    joint.text = text
    joint.append(child.getroot().find(cnode))

    return combined


def expandSubTree(node):

    print(node.tag, node.attrib, node.text)
    if len(node) > 0:
        for child in node:
            print (" "*2, '{:3d}'.format(len(child)), child.tag, child.attrib, child.text)


def expandSubTreeRecurse(node, depth=0):

    indent = " "*2
    if node != None:
        if depth == 0:
            print(indent*depth, node.tag, node.attrib, node.text)
        if len(node) > 0:
            for child in node:
                print (indent*(depth+1), '[{:2d}]'.format(len(child)), 
                       child.tag, child.attrib, child.text)
                expandSubTreeRecurse(child, depth=depth+1) 


def main():

    #glbfit_path = "/home/ycjang/work/CU-global-fit"
    #analy_path = "NNLO_chpt/run/su2/NLO/05-2018-rerun/ChPT/cut370MeV/no_chisq_weight/1gev"
    #xml_path = os.path.join(glbfit_path, analy_path, 'xml')
    xml_path = os.getcwd()
    
    ''' load xml templates '''
    GlobalFitArgs = LoadXML(xml_path + '/xml_base/GlobalFitArgs.xml')
    Lattices = LoadXML(xml_path + '/xml_base/Lattices.xml')
    Ensembles = LoadXML(xml_path + '/xml_base/Ensembles.xml')
    ValenceCombos = LoadXML(xml_path + '/xml_base/ValenceCombos.xml')
    mydata = LoadXML(xml_path + '/xml_base/mydata.xml')
    Freeze = LoadXML(xml_path + '/xml_base/Freeze.xml')
    Bind = LoadXML(xml_path + '/xml_base/Bind.xml')
    ExtraEnsembles = LoadXML(xml_path + '/xml_base/ExtraEnsembles.xml')
    
    ''' lattice parameters '''
    lattice_info = [ l32I.XMLWriter,
                     l24I.XMLWriter, 
                     l48I_M.XMLWriter, 
                     l64I_M.XMLWriter, 
                     l32Ifine.XMLWriter, 
                     l32ID.XMLWriter]

    ''' define fit constraints '''
    fit_spec = {'FitFunc': 'FitNLOSU2ChPTIPlusID',
                'freeze_info': [],
                'bind_info': [],
               }
    
    for param in [['RatioZl', '3', '1'], ['RatioZh', '3', '1'], \
                  ['GlobalFitParameter', '1', '0'], \
                  ['GlobalFitParameter', '2', '0'], \
                  ['GlobalFitParameter', '13', '0'], \
                  ['GlobalFitParameter', '14', '0'], \
                  ['GlobalFitParameter', '27', '0'], \
                  ['GlobalFitParameter', '28', '0'], \
                  ['GlobalFitParameter', '32', '0'], \
                  ['GlobalFitParameter', '33', '0'], \
                  ['GlobalFitParameter', '34', '0'], \
                  ['GlobalFitParameter', '35', '0'], \
                  ['GlobalFitParameter', '36', '0'], \
                  ['GlobalFitParameter', '37', '0'], \
                  ['GlobalFitParameter', '38', '0'], \
                  ['GlobalFitParameter', '39', '0'], \
                  ['GlobalFitParameter', '40', '0'], \
                  ['GlobalFitParameter', '41', '0'], \
                  ['GlobalFitParameter', '52', '0'], \
                  ['GlobalFitParameter', '53', '0'], \
                  ['GlobalFitParameter', '54', '0'], \
                  ['GlobalFitParameter', '55', '0']]:
        
        ffo = {'param_name': param[0],
               'param': param[1],
               'data': {'Constant': param[2],
                        'BootType': 'SuperJackBootResampledConstant',
                        'ParamFile': 'file.boot',
                        'ParamNumber': '0',
                       },
              }

        fit_spec['freeze_info'].append(ffo)

    for param in [['RatioZl', '2', '1'], ['RatioZh', '2', '1']]:

        bfo = {'param_name': param[0],
               'param_bind': param[1],
               'param_to'  : param[2],
               }

        fit_spec['bind_info'].append(bfo)

    ''' extra ensembles '''
    extra_ensemble_info = []
    
    for param in [['24I_fake_0.005', 147], \
                  ['24I_fake_0.01', 153], \
                  ['24I_fake_0.02', 85], \
                  ['24I_fake_0.03', 105], \
                  ['32I_fake_0.004', 135], \
                  ['32I_fake_0.006', 152], \
                  ['32I_fake_0.008', 120], \
                  ['fake_ZV_32ID', 50], \
                 ]:

        efo = {'PerBin': '1',
               'Boots': str(param[1]),
               'Resampling': 'Jackknife',
               'FOpenerArg': {'num_per_traj': '1',
                              'traj_start': '0',
                              'traj_increment': '1',
                              'traj_lessthan': str(param[1]),
                              'basis_shifts': '0',
                              'ensemble_tag': param[0],
                             },
              }

        extra_ensemble_info.append(efo)

    ''' build input xml tree '''
    glb = copy.deepcopy(GlobalFitArgs)
    
    glb.find('./FitFunc').text = fit_spec['FitFunc']

    glb.find('./NamedVariables').text = ''
   
    
    for ffo in fit_spec['freeze_info']:
        
        leaf = copy.deepcopy(Freeze)
        
        for key in ffo.keys():
          
            if key == 'data':
                for subkey in ffo[key].keys():
                    leaf.find('./elem/'+key+'/'+subkey).text = ffo[key][subkey]
            else:
                leaf.find('./elem/'+key).text = ffo[key]
    
        glb = joinTrees(glb, './Freeze', leaf, './elem', text='\n')

    
    for bfo in fit_spec['bind_info']:
        
        leaf = copy.deepcopy(Bind)
        
        for key in bfo.keys():
            leaf.find('./elem/'+key).text = bfo[key]
        
        glb = joinTrees(glb, './Bind', leaf, './elem', text='\n')
   

    for efo in extra_ensemble_info:
        
        leaf = copy.deepcopy(ExtraEnsembles)
        
        for key in efo.keys():
          
            if key == 'FOpenerArg':
                for subkey in efo[key].keys():
                    leaf.find('./elem/'+key+'/'+subkey).text = efo[key][subkey]
            else:
                leaf.find('./elem/'+key).text = efo[key]
    
        glb = joinTrees(glb, './ExtraEnsembles', leaf, './elem', text='\n')
   

    for lfo in lattice_info:

        lat = lfo.makeSectionLattice( Lattices, Ensembles, ValenceCombos, mydata )

        glb = joinTrees(glb, './Lattices', lat, './elem')
       

    ''' save the full tree to the file '''
    SaveXML(glb, xml_path + '/global_fit.xml')


if __name__ == '__main__':
    main()
