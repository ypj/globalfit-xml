#!/usr/bin/env python3

import xml.etree.ElementTree as ET
import copy



def joinTrees(parent, pnode, child, cnode, text=''):

    combined = copy.deepcopy(parent)
    joint = combined.getroot().find(pnode)
    joint.text = text
    joint.append(child.getroot().find(cnode))

    return combined



class LatticeInfoBase:

    def __init__(self, info):

        self.info = info
        #print(self.info)


    def makeSectionLattice(self, Lattices, Ensembles, ValenceCombos, mydata):

        info = self.info

        print('id', info['id'])

        lat = copy.deepcopy(Lattices)

        lat.find('./elem').set('id',info['id'])

        lat.find('./elem/Latt').text = str(info['Latt'][0]) + ' ' \
                                     + str(info['Latt'][1]) + ' ' \
                                     + str(info['Latt'][2]) + ' ' \
                                     + str(info['Latt'][3])
        
        lat.find('./elem/Action').text = info['Action'] 
        lat.find('./elem/ChisqWeight').text = str(info['ChisqWeight'])
        
        for im, mstr in enumerate(['start', 'increment', 'end', 'data']):
            lat.find('./elem/RWargs/mq_'+mstr).text = str(info['RWargs']['mq'][im])

        lat.find('./elem/mres_type').text = info['mres_type']
        
        for key in ['BootType', 'ParamFile', 'ParamNumber']:
          lat.find('./elem/mres_chiral/'+key).text = str(info['mres_chiral'][key])
        
        lat.find('./elem/plot_my_start').text = str(info['plot_my'][0])
        lat.find('./elem/plot_my_end').text = str(info['plot_my'][1])

        self.lat = lat

        self.appendSectionEnsemble( Ensembles, ValenceCombos, mydata )

        return self.lat
  

    def appendSectionEnsemble(self, Ensembles, ValenceCombos, mydata):

        info = self.info

        Ml_sea = info['Ensembles']['Ml_sea']
        EnsParams = info['Ensembles']['EnsParams']
        ValParams = info['Ensembles']['ValParams']
        
        for i in range(len(Ml_sea)):
            
            ens = copy.deepcopy(Ensembles)

            ens.find('./elem').attrib = {'id': info['id'], 
                                         'ml': str(Ml_sea[i]), 
                                         'ms': str(info['RWargs']['mq'][-1])}

            ens.find('./elem/Ml_sea').text = str(Ml_sea[i])

            for key in ['PerBin', 'Boots', 'Resampling']:
                ens.find('./elem/EnsParams/'+key).text = str(EnsParams[key])
            
            for key in ['ensemble_tag', 'traj_start', 'traj_increment', 'traj_lessthan']:
                if key == 'traj_increment':
                    ens.find('./elem/EnsParams/FOpenerArg/'+key).text = str(EnsParams['FOpenerArg'][key])
                else:
                    ens.find('./elem/EnsParams/FOpenerArg/'+key).text = str(EnsParams['FOpenerArg'][key][i])
            
            self.ens = ens
            
            for obs in ['Mpi', 'fpi', 'BK', 'MK', 'fK', 'MOmega', 'MPhi', 
                        'r0', 'r1', 'sqrt_t0', 'w0', 'a00', 'a02', 
                        'pi_ff', 'pir2', 'K_ff', 'Kr2', 'OVVpAA']:

                if obs in ValParams.keys():
                
                    if obs in ['Mpi', 'fpi']:
                        self.appendSectionPion(i, obs, ValenceCombos, mydata)
        
                    elif obs in ['MK', 'fK']:
                        self.appendSectionKaon(i, obs, ValenceCombos, mydata)
                    
                    elif obs in ['MOmega']:
                        self.appendSectionOmega(i, obs, ValenceCombos, mydata)
                    
                    elif obs in ['sqrt_t0', 'w0']:
                        self.appendSectionWilsonFlow(i, obs, ValenceCombos, mydata)
                    
                    elif obs in ['a02']:
                        self.appendSectionPionScattering(i, obs, ValenceCombos, mydata)
    
                else:
                    self.appendSectionNoMeasurement(i, obs)
            
            self.lat = joinTrees(self.lat, './elem/Ensembles', self.ens, './elem')
    
    
    def appendSectionPion(self, i, obs, ValenceCombos, mydata):

        info = self.info

        Ml_sea = info['Ensembles']['Ml_sea']
        ValParams = info['Ensembles']['ValParams']
            
        mlcut = ValParams['mlcuts'][i]
        mhcut = ValParams['mhcuts'][i]
        
        params = ValParams[obs]
        masses = ValParams['mcombo']
        for Mx in masses:
            vc_Mx = copy.deepcopy(ValenceCombos)
            vc_Mx.find('./elem/Mx').text = str(Mx)
            
            for My in masses:
                if Mx>=My:
                    vc_My = copy.deepcopy(mydata)
                    vc_My.find('./elem/My').text = str(My)
                    ParamFile = params['ParamFile'].replace('@MLSEA', str(Ml_sea[i]))\
                                                   .replace('@MX', str(Mx))\
                                                   .replace('@MY', str(My))
                    vc_My.find('./elem/boot/ParamFile').text = ParamFile
                    for opt in ['BootType', 'ParamNumber', 'Constant']:
                        vc_My.find('./elem/boot/'+opt).text = params[opt]
                    if Mx > mlcut or My > mlcut:
                        vc_My.find('./elem/include_in_fit').text = 'false'
                    else:
                        vc_My.find('./elem/include_in_fit').text = 'true'
                    
                    vc_Mx = joinTrees(vc_Mx, './elem/mydata', vc_My, './elem')
        
            self.ens = joinTrees(self.ens, './elem/'+obs+'_data', vc_Mx, './elem')
        
    
    def appendSectionKaon(self, i, obs, ValenceCombos, mydata):

        info = self.info

        Ml_sea = info['Ensembles']['Ml_sea']
        ValParams = info['Ensembles']['ValParams']
            
        mlcut = ValParams['mlcuts'][i]
        mhcut = ValParams['mhcuts'][i]

        params = ValParams[obs]
        for Mx in ValParams['mh']:
            vc_Mx = copy.deepcopy(ValenceCombos)
            vc_Mx.find('./elem/Mx').text = str(Mx)
            
            for My in ValParams['ml']:
                vc_My = copy.deepcopy(mydata)
                vc_My.find('./elem/My').text = str(My)
                ParamFile = params['ParamFile'].replace('@MLSEA', str(Ml_sea[i]))\
                                               .replace('@MX', str(Mx))\
                                               .replace('@MY', str(My))
                vc_My.find('./elem/boot/ParamFile').text = ParamFile
                for opt in ['BootType', 'ParamNumber', 'Constant']:
                    vc_My.find('./elem/boot/'+opt).text = params[opt]
                if My > mlcut:
                    vc_My.find('./elem/include_in_fit').text = 'false'
                else:
                    vc_My.find('./elem/include_in_fit').text = 'true'
                
                vc_Mx = joinTrees(vc_Mx, './elem/mydata', vc_My, './elem')
                
            self.ens = joinTrees(self.ens, './elem/'+obs+'_data', vc_Mx, './elem')
            
    
    def appendSectionOmega(self, i, obs, ValenceCombos, mydata):

        info = self.info

        Ml_sea = info['Ensembles']['Ml_sea']
        ValParams = info['Ensembles']['ValParams']
            
        mlcut = ValParams['mlcuts'][i]
        mhcut = ValParams['mhcuts'][i]

        params = ValParams[obs]
        for Mx in ValParams['mh']:
            vc_Mx = copy.deepcopy(ValenceCombos)
            vc_Mx.find('./elem/Mx').text = str(Mx)
            vc_My = copy.deepcopy(mydata)
            vc_My.find('./elem/My').text = str(Mx)
            franges = params['franges']
            ParamFile = params['ParamFile'].replace('@MLSEA', str(Ml_sea[i]))\
                                           .replace('@MX', str(Mx))\
                                           .replace('@FRMIN', str(franges[i][0]))\
                                           .replace('@FRMAX', str(franges[i][1]))
            vc_My.find('./elem/boot/ParamFile').text = ParamFile
            for opt in ['BootType', 'ParamNumber', 'Constant']:
                vc_My.find('./elem/boot/'+opt).text = params[opt]
            if Mx > mhcut:
                vc_My.find('./elem/include_in_fit').text = 'false'
            else:
                vc_My.find('./elem/include_in_fit').text = 'true'
                
            vc_Mx = joinTrees(vc_Mx, './elem/mydata', vc_My, './elem')
            self.ens = joinTrees(self.ens, './elem/'+obs+'_data', vc_Mx, './elem')
            
    
    def appendSectionWilsonFlow(self, i, obs, ValenceCombos, mydata):

        info = self.info

        Ml_sea = info['Ensembles']['Ml_sea']
        ValParams = info['Ensembles']['ValParams']
            
        mlcut = ValParams['mlcuts'][i]
        mhcut = ValParams['mhcuts'][i]

        params = ValParams[obs]
        for Mx in [info['RWargs']['mq'][3]]:
            vc_Mx = copy.deepcopy(ValenceCombos)
            vc_Mx.find('./elem/Mx').text = str(Mx)
            vc_My = copy.deepcopy(mydata)
            vc_My.find('./elem/My').text = str(Mx)
            ParamFile = params['ParamFile'].replace('@MLSEA', str(Ml_sea[i]))
            vc_My.find('./elem/boot/ParamFile').text = ParamFile
            for opt in ['BootType', 'ParamNumber', 'Constant']:
                vc_My.find('./elem/boot/'+opt).text = params[opt]
            if Mx > mhcut:
                vc_My.find('./elem/include_in_fit').text = 'false'
            else:
                vc_My.find('./elem/include_in_fit').text = 'true'
            
            vc_Mx = joinTrees(vc_Mx, './elem/mydata', vc_My, './elem')
            self.ens = joinTrees(self.ens, './elem/'+obs+'_data', vc_Mx, './elem')
            
    
    def appendSectionPionScattering(self, i, obs, ValenceCombos, mydata):

        info = self.info

        Ml_sea = info['Ensembles']['Ml_sea']
        ValParams = info['Ensembles']['ValParams']
            
        mlcut = ValParams['mlcuts'][i]
        mhcut = ValParams['mhcuts'][i]
        
        params = ValParams[obs]
        for Mx in [Ml_sea[i]]:
            vc_Mx = copy.deepcopy(ValenceCombos)
            vc_Mx.find('./elem/Mx').text = str(Mx)
            vc_My = copy.deepcopy(mydata)
            vc_My.find('./elem/My').text = str(Mx)
            ParamFile = params['ParamFile'].replace('@MLSEA0p', str(Mx).replace('.','p'))
            vc_My.find('./elem/boot/ParamFile').text = ParamFile
            for opt in ['BootType', 'ParamNumber', 'Constant']:
                vc_My.find('./elem/boot/'+opt).text = params[opt]
            if Mx > mlcut:
                vc_My.find('./elem/include_in_fit').text = 'false'
            else:
                vc_My.find('./elem/include_in_fit').text = 'true'
            
            vc_Mx = joinTrees(vc_Mx, './elem/mydata', vc_My, './elem')
            self.ens = joinTrees(self.ens, './elem/'+obs+'_data', vc_Mx, './elem')
    
    
    def appendSectionNoMeasurement(self, i, obs):

        self.ens.find('./elem/'+obs+'_data').text = ''
